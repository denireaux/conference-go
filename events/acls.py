import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


#when called, get an image for a specific city and state
def get_photo(city, state):

    #associates api with request to be made
    header = {"Authorization" : PEXELS_API_KEY}

    #creates parameter object for the parameters we want to use
    params = {
        "query" : city + " " + state,
        "per_page" : 1,
    }

    #search page url for pexels
    url = "https://api.pexels.com/v1/search"

    #response
    response = requests.get(url, params=params, headers=header)

    content = response.json()

    try:
        return {"picture_url" : content["photos"][0]["src"]["original"]}
    
    except (KeyError, IndexError):
        return {"picture_url" : None}
    

#when called converts city, state to lat, long and gets weather for that conferences location
def get_weather_data(city, state):

    #creates parameter object for the parameters we want to use
    params = {"q" : f"{city}, {state}", "appid" : OPEN_WEATHER_API_KEY}

    #url for lat long
    url = "http://api.openweathermap.org/geo/1.0/direct"

    #response for latlong
    response = requests.get(url, params=params)
    content = response.json()

    lat = content[0]["lat"]
    lon = content[0]["lon"]
    

    #for the actual weather data
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {"lat" : lat, "lon" : lon, "appid" : OPEN_WEATHER_API_KEY, "units" : "imperial"}
    
    #making the request
    response = requests.get(url, params=params)
    content = response.json()

    #getting the description
    weather_description = content["weather"][0]["description"]
    temperature = content["main"]["temp"]

    return {"weather_description" : weather_description, "temperature" : temperature}